import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AdminComponent } from './admin.component';
import { adminRoutes } from './admin.routes';
import { ToolsModule } from '../tools/tools.module';

@NgModule({
  declarations: [
    AdminComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(adminRoutes),
    ToolsModule
  ]
})
export class AdminModule { }
