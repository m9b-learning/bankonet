import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  message: string;
  returnUrl: string;
  error: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public authService: AuthService
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      userId: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.authService.logout();
  }

  get f() {
    return this.loginForm.controls;
  }

  login() {
    console.log('login called');
    if (this.loginForm.invalid) {
      return;
    }

    if (this.f.userId.value == "admin") {
      this.authService.loginAdmin();

      this.router.navigate(['/admin']);
    }
    else {
      this.authService.login(this.f.userId.value, this.f.password.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['/client']);
          },
          error => {
            this.error = error;
          });
    }

  }
}
