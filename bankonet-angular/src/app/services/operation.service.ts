import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Operation } from '../models/operation';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OperationService {

  baseUrl: string = "http://localhost:8080/bankonet";
  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json'
    })
  };

  getOperationsByAccountId(id: number) {
    return this.http.get<Operation[]>(this.baseUrl + `/operation/account/${id}`, this.httpOptions).pipe(
      map(
        (jsonArray: Object[]) => jsonArray.map(jsonItem => Operation.fromJson(jsonItem))
      ));
  }

  createOperation(operation: Operation): Observable<Operation> {
    return this.http.post<Operation>(this.baseUrl + "/operation", operation, this.httpOptions)
  }
}
