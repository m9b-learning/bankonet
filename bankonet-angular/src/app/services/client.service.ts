import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Client } from '../models/client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  baseUrl: string = "http://localhost:8080/bankonet";
  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json'
    })
  };

  getClients() {
    return this.http.get<Client[]>(this.baseUrl + "/client", this.httpOptions).pipe(
      map(
        (jsonArray: Object[]) => jsonArray.map(jsonItem => Client.fromJson(jsonItem))
      ));
  }

  createClient(client: Client): Observable<Client> {
    return this.http.post<Client>(this.baseUrl + "/client", client, this.httpOptions)
  }

}
