import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Account } from '../models/account';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  baseUrl: string = "http://localhost:8080/bankonet";
  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json'
    })
  };

  getAccounts() {
    return this.http.get<Account[]>(this.baseUrl + "/account", this.httpOptions).pipe(
      map(
        (jsonArray: Object[]) => jsonArray.map(jsonItem => Account.fromJson(jsonItem))
      ));
  }

  getAccountById(id: number) {
    return this.http.get<Account>(this.baseUrl + `/account/${id}`, this.httpOptions);
  }

  getCurrentAccountByClientId(id: number) {
    return this.http.get<Account>(this.baseUrl + `/account/client/current/${id}`, this.httpOptions);
  }

  getAccountsByIdClient(id: number) {
    return this.http.get<Account[]>(this.baseUrl + `/account/client/${id}`, this.httpOptions).pipe(
      map(
        (jsonArray: Object[]) => jsonArray.map(jsonItem => Account.fromJson(jsonItem))
      ));
  }

  createAccount(account: Account) {
    return this.http.post<Account>(this.baseUrl + "/account", account, this.httpOptions)
  }
}
