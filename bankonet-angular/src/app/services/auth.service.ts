import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl: string = "http://localhost:8080/bankonet";
  httpOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  login(email: string, password: string) {
    return this.http.post<any>(this.baseUrl + `/client/signin`, JSON.stringify({ email, password }), this.httpOptions )
      .pipe(map(user => {
        if (user) {
          console.log(user);
          localStorage.setItem('currentUser', JSON.stringify(user));
          localStorage.setItem('isLoggedIn', 'true');
        }
        return user;
      }));
  }

  loginAdmin() {
    //En dur, à la degueulasse !
    localStorage.setItem('currentUser', JSON.stringify({ "firstname": "Agent", "lastname": "Smith", "email": "hugo.weaving@trix.com", "phone": "01 02 03 04 05", "address": "23 avenue Léonard De Vinci", "city": "PESSAC", "postalCode": "33600", "country": "FRANCE" }));
    localStorage.setItem('isLoggedIn', 'true');
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    localStorage.removeItem('isLoggedIn');
  }
}
