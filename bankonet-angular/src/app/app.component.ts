import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { StorageService, KEY_LOCALE } from './services/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Bankonet';
  locale: string;

  constructor(
    private translate: TranslateService,
    private storageService: StorageService
  ) {
    this.locale = this.storageService.get(KEY_LOCALE) || 'fr';
    this.translate.setDefaultLang('en');
    this.translate.use(this.locale);
  }

  evt(val: number) {
    console.log('AppComponent:ALERT', val);
  }

  setLocale(locale: string): void {
    this.storageService.set(KEY_LOCALE, locale);
    this.translate.use(locale);
    this.locale = locale;
  }
}
