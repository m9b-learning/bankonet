import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { StorageService, KEY_LOCALE  } from '../../services/storage.service';
import { first } from 'rxjs/operators';
import { Client } from '../../models/client';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  client: Client;
  locale: string;

  constructor(private router: Router,
    public authService: AuthService,
    private storageService: StorageService,
    private translate: TranslateService,) {
    this.locale = this.storageService.get(KEY_LOCALE) || 'fr';
    this.translate.setDefaultLang('en');
    this.translate.use(this.locale);
     }

  ngOnInit() {
  }

  logout(): void {
    console.log('logout');
    this.authService.logout();
    this.router.navigate(['/']);
  }
  setLocale(locale: string): void {
    this.storageService.set(KEY_LOCALE, locale);
    this.translate.use(locale);
    this.locale = locale;
  }

}
