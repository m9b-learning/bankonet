import { Component, OnInit } from '@angular/core';
import { StorageService } from '../services/storage.service';
import { Client } from '../models/client';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  client: Client;
  constructor(private storageService: StorageService) {
    this.client = JSON.parse(storageService.get('currentUser'));
  }
  ngOnInit() {
  }

}
