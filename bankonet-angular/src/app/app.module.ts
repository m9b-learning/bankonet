import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LOCALE_ID, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { AppComponent } from './app.component';
import { ToolsModule } from './tools/tools.module';
import { LogInterceptorService } from './services/log-interceptor.service';
import { appRoutes } from './app.routes';
import { translateOptions } from './app.translate';
import { ClientModule } from './client/client.module';
import { LayoutComponent } from './layout/layout.component';
import { FooterComponent } from './layout/footer/footer.component';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    FooterComponent,
    NavbarComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    ClientModule,
    ToolsModule,
    RouterModule.forRoot(appRoutes),
    TranslateModule.forRoot(translateOptions)
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: LogInterceptorService, multi: true },
    { provide: LOCALE_ID, useValue: 'fr' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
