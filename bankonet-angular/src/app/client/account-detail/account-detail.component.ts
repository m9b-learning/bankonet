import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AccountService } from 'src/app/services/account.service';
import { OperationService } from 'src/app/services/operation.service';
import { Operation } from 'src/app/models/operation';
import { Account } from 'src/app/models/account';

@Component({
  selector: 'app-account-detail',
  templateUrl: './account-detail.component.html',
  styleUrls: ['./account-detail.component.scss']
})
export class AccountDetailComponent implements OnInit {
  operations: Operation[];
  account: Account;
  error: string;

  constructor(private accountService: AccountService,
    private operationService: OperationService,
    private route: ActivatedRoute) {

    const id = +(this.route.snapshot.params.id);

    this.accountService.getAccountById(id)
    .subscribe(
      account => { this.account = account },
        error => { this.error = error.message }
      );

    this.operationService.getOperationsByAccountId(id)
    .subscribe(
      operations => { this.operations = operations },
        error => { this.error = error.message }
      );
  }

  ngOnInit() {
  }

}
