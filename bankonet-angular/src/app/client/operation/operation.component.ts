import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Operation } from 'src/app/models/operation';
import { Account } from 'src/app/models/account';
import { AccountService } from 'src/app/services/account.service';
import { OperationService } from 'src/app/services/operation.service';

@Component({
  selector: 'app-operation',
  templateUrl: './operation.component.html',
  styleUrls: ['./operation.component.scss']
})
export class OperationComponent implements OnInit {
  operation = new Operation();
  account = new Account();
  error: string;
  submitted = false;

  constructor(private accountService: AccountService,
    private operationService: OperationService,
    private route: ActivatedRoute,
    private router: Router) {
    this.operation.accountId = +(this.route.snapshot.params.id);
    const id = +(this.route.snapshot.params.id);

    this.accountService.getAccountById(id)
      .subscribe(
        account => { this.account = account },
        error => { this.error = error.message }
      );
  }

  ngOnInit() {
  }

  onSubmit(form) {

    // /!\ traitement qui pue, il faut faire les controles !
    // Est-ce que le solde permet le virement ?
    // Dans le cas d'un compte courant, ne pas dépasser le découvert autorisé
    // Dans le cas d'un compte épargne, ne pas dépasser le solde minimum

    this.submitted = true;
    if (form.valid) {

      this.submitted = true;

      let seuil = ((this.account.balance + this.account.overdraft) - this.account.minimumBalance);
      if ((seuil - this.operation.amount) < 0) {
        this.error = 'Fonds disponibles insuffisants';
        return;
      }

      this.operationService.createOperation(this.operation)
        .subscribe(
          result => {
            this.router.navigate(['../..'], {
              relativeTo: this.route
            });
          },
          error => {
            this.error = error.message;
            this.ngOnInit();
          }
        );
    }
  }
}
