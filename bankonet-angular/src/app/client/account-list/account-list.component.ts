import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { AccountService } from 'src/app/services/account.service';
import { StorageService } from 'src/app/services/storage.service';
import { Client } from 'src/app/models/client';
import { Account } from 'src/app/models/account';

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.scss']
})
export class AccountListComponent implements OnInit {

  accounts: Account[];
  client: Client;
  error: string;
  @Input() account: Account;
  @Output() notify: EventEmitter<number> = new EventEmitter();

  constructor(private accountService: AccountService,
    private storageService: StorageService) {

    this.client = JSON.parse(storageService.get('currentUser'));
    this.accountService.getAccountsByIdClient(this.client.id).subscribe(
      accounts => { this.accounts = accounts },
      error => { this.error = error.message; }
    );
  }

  ngOnInit() {
  }

  onClick() {
    this.notify.emit(this.account.id);
  }
}
