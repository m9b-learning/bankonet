import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr, 'fr');

import { ClientComponent } from './client.component';
import { ToolsModule } from '../tools/tools.module';
import { clientRoutes } from './client.routes';
import { AccountListComponent } from './account-list/account-list.component';
import { AccountDetailComponent } from './account-detail/account-detail.component';
import { OperationComponent } from './operation/operation.component';
import { TransferComponent } from './transfer/transfer.component';

@NgModule({
  declarations: [ClientComponent, AccountListComponent, AccountDetailComponent, OperationComponent, TransferComponent],
  imports: [
    CommonModule,
    ToolsModule,
    RouterModule.forChild(clientRoutes)
  ]
})
export class ClientModule { }
