import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm, Validators } from '@angular/forms'
import { ClientService } from 'src/app/services/client.service';
import { AccountService } from 'src/app/services/account.service';
import { Client } from 'src/app/models/client';
import { Account } from 'src/app/models/account';
import { StorageService } from 'src/app/services/storage.service';
import { Operation } from 'src/app/models/operation';
import { OperationService } from 'src/app/services/operation.service';
import { Transfer } from './transfer';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss']
})
export class TransferComponent implements OnInit {
  @ViewChild('selAccount') selAccount: ElementRef;
  @ViewChild('selClient') selClient: ElementRef;
  @ViewChild('transferForm') formulaire: NgForm;

  accounts: Account[];
  clients: Client[];
  client: Client;
  transfer = new Transfer();
  error: string;
  submitted = false;
  accountTo: Account;
  accountFrom: Account;
  operationFrom = new Operation();
  operationTo = new Operation();

  constructor(private accountService: AccountService,
    private clientService: ClientService,
    private operationService: OperationService,
    private storageService: StorageService,
    private route: ActivatedRoute,
    private router: Router) {

    this.client = JSON.parse(storageService.get('currentUser'));

    this.accountService.getAccountsByIdClient(this.client.id).subscribe(
      accounts => { this.accounts = accounts; },
      error => { this.error = error.message; }
    );

    this.clientService.getClients().subscribe(
      clients => { this.clients = clients },
      error => { this.error = error.message; }
    );
  }

  ngOnInit() {
  }

  searchAccountTo(id: number, from: string) {

    if (from == 'intern') {
      this.accountTo = this.accounts.filter(acc => acc.id == id)[0];
    } else {
      this.accountService.getCurrentAccountByClientId(id).subscribe(
        accounts => { this.accountTo = accounts; },
        error => { this.error = error.message; }
      );
    }

  }

  onSubmit(form) {

    // /!\ traitement qui pue, il faut faire les controles !
    // Est-ce que le solde permet le virement ?
    // Dans le cas d'un compte courant, ne pas dépasser le découvert autorisé
    // Dans le cas d'un compte épargne, ne pas dépasser le solde minimum
    // Est-ce que les comptes sont bien différents ?
    // rediriger vers la page liste des comptes une fois le virement validé

    this.submitted = true;
    if (form.valid) {

      this.accountFrom = this.accounts.filter(acc => acc.id == this.transfer.fromId)[0];
      let seuil = ((this.accountFrom.balance + this.accountFrom.overdraft) - this.accountFrom.minimumBalance);

      if ( (seuil - this.transfer.amount )< 0 ) {
        this.error = 'Fonds disponibles insuffisants';
        return;
      }

      if (this.accountFrom.id == this.accountTo.id) {
        this.error = 'Vous devez choisir un compte de destination différent du compte expéditeur';
        return;
      }


      this.operationFrom.accountId = this.accountFrom.id;
      this.operationFrom.type = 'debit';
      this.operationFrom.amount = this.transfer.amount;
      this.operationFrom.label = '[Virement à destination du compte n° ' + this.accountTo.label + '] ' + this.transfer.label;

      this.operationTo.accountId = this.accountTo.id;
      this.operationTo.type = 'credit';
      this.operationTo.amount = this.transfer.amount;
      this.operationTo.label = '[Virement en provenance du compte n° ' + this.accountFrom.label + '] ' + this.transfer.label;

      //*/
      this.operationService.createOperation(this.operationFrom)
        .subscribe(
          result => {},
          error => {
            this.error = error.message;
            this.ngOnInit();
          }
        );

      this.operationService.createOperation(this.operationTo)
        .subscribe(
          result => {
            this.router.navigate(['..'], {
              relativeTo: this.route
            });
          },
          error => {
            this.error = error.message;
            this.ngOnInit();
          }
        );
        //*/
    }
  }

  selectAccount(ref: any) {

    //ça marche très moyennement ça !
    this.transfer = new Transfer();

    if (ref == "account") {
      this.selAccount.nativeElement.disabled = false;
      this.selAccount.nativeElement.hidden = false;

      this.formulaire.form.controls.toAccount.setValidators([Validators.required]);
      this.formulaire.form.controls.toAccount.updateValueAndValidity();

      this.selClient.nativeElement.disabled = true;
      this.selClient.nativeElement.hidden = true;
      this.formulaire.form.controls.toClient.clearValidators();
      this.formulaire.form.controls.toClient.updateValueAndValidity();

    } else {
      this.selAccount.nativeElement.disabled = true;
      this.selAccount.nativeElement.hidden = true;
      this.formulaire.form.controls.toAccount.clearValidators();
      this.formulaire.form.controls.toAccount.updateValueAndValidity();

      this.selClient.nativeElement.disabled = false;
      this.selClient.nativeElement.hidden = false;
      this.formulaire.form.controls.toClient.setValidators([Validators.required]);
      this.formulaire.form.controls.toClient.updateValueAndValidity();
    }


  }

}
