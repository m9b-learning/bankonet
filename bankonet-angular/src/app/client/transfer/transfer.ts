export class Transfer {

    fromId?: number;
    toAccount?: number;
    toClient?: number;
    toType?: number;
    amount?: number;
    label?: string;
}