import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { StorageService } from '../services/storage.service';
import { Client } from '../models/client';
import { ClientService } from '../services/client.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
  @ViewChild('btnClose') btnClose: ElementRef;
  @ViewChild('passwordRef') password: ElementRef;
  client: Client;
  editClient: Client;
  submitted = false;
  error: string;

  constructor(private storageService: StorageService,
    private clientService: ClientService,
    private route: ActivatedRoute,
    private router: Router) {
    this.client = JSON.parse(storageService.get('currentUser'));
    this.editClient = JSON.parse(storageService.get('currentUser'));
  }

  ngOnInit() {
  }

  onSubmit(form) {
    console.log(this.client);
    this.submitted = true;
    if (form.valid) {
      this.clientService.createClient(this.editClient)
        .subscribe(
          result => {
            this.client.firstname = this.editClient.firstname;
            this.client.lastname = this.editClient.lastname;
            this.client.address = this.editClient.address;
            this.client.postalCode = this.editClient.postalCode;
            this.client.city = this.editClient.city;
            this.client.phone = this.editClient.phone;
            this.client.email = this.editClient.email

            localStorage.setItem('currentUser', JSON.stringify(this.client));

            this.password.nativeElement.value = '';
            this.btnClose.nativeElement.click();
          },
          error => {
            this.error = error.message;
            this.ngOnInit();
          }
        );
    }
  }

  cancelModal() {
    this.editClient = JSON.parse(this.storageService.get('currentUser'));
  }
}
