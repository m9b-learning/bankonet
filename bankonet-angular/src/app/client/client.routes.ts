import { Routes } from '@angular/router';
import { ClientComponent } from './client.component';
import { AccountListComponent } from './account-list/account-list.component';
import { AccountDetailComponent } from './account-detail/account-detail.component';
import { AuthGuard } from '../login/guard/auth.guard';
import { OperationComponent } from './operation/operation.component';
import { TransferComponent } from './transfer/transfer.component';

export const clientRoutes: Routes = [{
    path: 'client',
    component: ClientComponent ,
    children: [
      { path: '', component: AccountListComponent, canActivate: [AuthGuard] },
      { path: 'account/:id', component: AccountDetailComponent, canActivate: [AuthGuard] },
      { path: 'operation/:id', component: OperationComponent, canActivate: [AuthGuard] },
      { path: 'transfer', component: TransferComponent, canActivate: [AuthGuard] }
    ]
  }];
