import { UppercaseDirective } from './uppercase.directive';
import { ElementRef } from '@angular/core';

const element = new ElementRef({
  style: {}
});

describe('UppercaseDirective', () => {
  it('should create an instance', () => {
    const directive = new UppercaseDirective(element);
    expect(directive).toBeTruthy();
  });
});
