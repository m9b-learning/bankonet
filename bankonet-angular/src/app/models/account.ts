import { Client } from './client';

export class Account {

    constructor(
        public id?: number,
        public clientId?: number,
        public label?: string,
        public overdraft?: number,
        public interestRate?: number,
        public minimumBalance?: number,
        public balance?: number,
        public type?: string,
        public dateCreate?: Date
    ) {}

    public static fromJson(json: Object): Account {
        return new Account(
            json['id'],
            json['clientId'],
            json['label'],
            json['overdraft'],
            json['interestRate'],
            json['minimumBalance'],
            json['balance'],
            json['type'],
            new Date(json['dateCreate'])
        );
    }
}