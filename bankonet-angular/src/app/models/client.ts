export class Client {

    constructor(
        public id: number,
        public firstname: string,
        public lastname: string,
        public email: string,
        public phone: string,
        public address: string,
        public city: string,
        public postalCode: string,
        public country: string,
        public dateCreate: Date
    ) { }

    public static fromJson(json: Object): Client {
        return new Client(
            json['id'],
            json['firstname'],
            json['lastname'],
            json['email'],
            json['phone'],
            json['address'],
            json['city'],
            json['postalCode'],
            json['country'],
            new Date(json['dateCreate'])
        );
    }
}