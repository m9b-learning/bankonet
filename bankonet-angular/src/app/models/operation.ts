import { Account } from './account';

export class Operation {

    constructor(
        public id?: number,
        public accountId?: number,
        public label?: string,
        public type?: string,
        public amount?: number,
        public dateCreate?: Date
    ) {}

    public static fromJson(json: Object): Operation {
        return new Operation(
            json['id'],
            json['accountId'],
            json['label'],
            json['type'],
            json['amount'],
            new Date(json['dateCreate'])
        );
    }
}