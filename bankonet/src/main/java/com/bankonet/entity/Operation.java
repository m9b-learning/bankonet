package com.bankonet.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NonNull;

@Entity
@Data
@Proxy(lazy=false)
public class Operation {

	public static final String TYPE_CREDIT = "credit";
	public static final String TYPE_DEBIT = "debit";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(nullable = false)
	private int accountId;
	
	@Column(nullable = false)
	private String label;

	@Column(nullable = false)
	private String type;

	@Column(nullable = false)
	private double amount;

	@Column(name = "datecreate")
	private Date dateCreate;

}
