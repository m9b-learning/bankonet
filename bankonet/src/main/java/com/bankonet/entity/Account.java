package com.bankonet.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Proxy;

import lombok.Data;

@Entity
@Data
@Proxy(lazy=false)
public class Account {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private int clientId;

	@Column(nullable = false)
	private String label;

	@Column(nullable = false)
	private double overdraft;

	@Column(name = "interestrate", nullable = false)
	private double interestRate;

	@Column(name = "minimumbalance", nullable = false)
	private double minimumBalance;

	private String type;

	@Column(name = "datecreate")
	private Date dateCreate;
}
