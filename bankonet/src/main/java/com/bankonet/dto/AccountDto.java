package com.bankonet.dto;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.bankonet.entity.Operation;
import com.bankonet.service.AccountService;

import lombok.Data;

@Data
public class AccountDto {
	private int id;
	private int clientId;
	private String label;
	private double overdraft;
	private double interestRate;
	private double minimumBalance;
	private String type;
	
	private double balance;
	private List<OperationDto> operations = new ArrayList<>();
	
	@JsonIgnore
	private Date dateCreate;
	
	public double getBalance() {
		if (operations.isEmpty()) {
			return 0.0;
		}
		
		BigDecimal totalCredit = new BigDecimal(operations.stream()
                			.filter(o -> o.getType().equals(Operation.TYPE_CREDIT))
            				.mapToDouble(o -> o.getAmount()).sum());
		
		BigDecimal totalDebit = new BigDecimal(operations.stream()
			    			.filter(o -> o.getType().equals(Operation.TYPE_DEBIT))
							.mapToDouble(o -> o.getAmount()).sum());

		return totalCredit.subtract(totalDebit).setScale(2 ,BigDecimal.ROUND_HALF_EVEN).doubleValue();
	}
}