package com.bankonet.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class OperationDto {

	private int id;
	private int accountId;
	private String label;
	private String type;
	private double amount;
	private Date dateCreate;
	
}
