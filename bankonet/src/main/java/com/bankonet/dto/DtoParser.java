package com.bankonet.dto;

import java.util.ArrayList;
import java.util.List;

import com.bankonet.entity.Account;
import com.bankonet.entity.Client;
import com.bankonet.entity.Operation;

public class DtoParser {


	///////////////////////////////////////////////////////////////////
	// CLIENT PARSER //////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////
	public static ClientDto parseClientToDto(Client client) {
		ClientDto clientDto = new ClientDto();
		clientDto.setId(client.getId());
		clientDto.setFirstname(client.getFirstname());
		clientDto.setLastname(client.getLastname());
		clientDto.setEmail(client.getEmail());
		clientDto.setPhone(client.getPhone());
		clientDto.setAddress(client.getAddress());
		clientDto.setPostalCode(client.getPostalCode());
		clientDto.setCity(client.getCity());
		clientDto.setCountry(client.getCountry());

		return clientDto;
	}

	public static Client parseDtoToClient(ClientDto clientDto) {
		Client client = new Client();
		client.setId(clientDto.getId());
		client.setFirstname(clientDto.getFirstname());
		client.setLastname(clientDto.getLastname());
		client.setEmail(clientDto.getEmail());
		client.setPhone(clientDto.getPhone());
		client.setAddress(clientDto.getAddress());
		client.setPostalCode(clientDto.getPostalCode());
		client.setCity(clientDto.getCity());
		client.setCountry(clientDto.getCountry());
		client.setPassword(clientDto.getPassword());


		return client;
	}

	public static List<ClientDto> parseListClientToDto(List<Client> clients) {
		List<ClientDto> dtoList = new ArrayList<ClientDto>();
		clients.forEach(cli -> dtoList.add(parseClientToDto(cli)));
		return dtoList;
	}

	public static List<Client> parseListDtoToClient(List<ClientDto> clientsDto) {
		List<Client> list = new ArrayList<Client>();
		clientsDto.forEach(cli -> list.add(parseDtoToClient(cli)));
		return list;
	}

	////////////////////////////////////////////////////////////////////
	// ACCOUNT PARSER //////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////
	public static AccountDto parseAccountToDto(Account account) {
		AccountDto accountDto = new AccountDto();
		accountDto.setId(account.getId());
		accountDto.setClientId(account.getClientId());
		accountDto.setLabel(account.getLabel());
		accountDto.setOverdraft(account.getOverdraft());
		accountDto.setInterestRate(account.getInterestRate());
		accountDto.setMinimumBalance(account.getMinimumBalance());
		accountDto.setType(account.getType());
		accountDto.setDateCreate(account.getDateCreate());
		
		return accountDto;
	}
	

	public static Account parseDtoToAccount(AccountDto accountDto) {
		Account account = new Account();
		account.setId(accountDto.getId());
		account.setClientId(accountDto.getClientId());
		account.setLabel(accountDto.getLabel());
		account.setOverdraft(accountDto.getOverdraft());
		account.setInterestRate(accountDto.getInterestRate());
		account.setMinimumBalance(accountDto.getMinimumBalance());
		account.setType(accountDto.getType());
		account.setDateCreate(accountDto.getDateCreate());

		return account;
	}

	public static List<AccountDto> parseListAccountToDto(List<Account> accounts) {
		List<AccountDto> dtoList = new ArrayList<AccountDto>();
		accounts.forEach(cli -> dtoList.add(parseAccountToDto(cli)));
		return dtoList;
	}

	public static List<Account> parseListDtoToAccount(List<AccountDto> accountsDto) {
		List<Account> list = new ArrayList<Account>();
		accountsDto.forEach(cli -> list.add(parseDtoToAccount(cli)));
		return list;
	}

	////////////////////////////////////////////////////////////////////
	// OPERATION PARSER ////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////
	public static OperationDto parseOperationToDto(Operation operation) {
		OperationDto operationDto = new OperationDto();
		operationDto.setId(operation.getId());
		operationDto.setAccountId(operation.getAccountId());
		operationDto.setLabel(operation.getLabel());
		operationDto.setType(operation.getType());
		operationDto.setAmount(operation.getAmount());
		operationDto.setDateCreate(operation.getDateCreate());

		return operationDto;
	}

	public static Operation parseDtoToOperation(OperationDto operationDto) {
		Operation operation = new Operation();

		operation.setId(operationDto.getId());
		operation.setAccountId(operationDto.getAccountId());
		operation.setLabel(operationDto.getLabel());
		operation.setType(operationDto.getType());
		operation.setAmount(operationDto.getAmount());
		operation.setDateCreate(operationDto.getDateCreate());

		return operation;
	}

	public static List<OperationDto> parseListOperationToDto(List<Operation> operations) {
		List<OperationDto> dtoList = new ArrayList<OperationDto>();
		operations.forEach(cli -> dtoList.add(parseOperationToDto(cli)));
		return dtoList;
	}

	public static List<Operation> parseListDtoToOperation(List<OperationDto> operationsDto) {
		List<Operation> list = new ArrayList<Operation>();
		operationsDto.forEach(cli -> list.add(parseDtoToOperation(cli)));
		return list;
	}

}
