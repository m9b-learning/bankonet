package com.bankonet.dto;

import java.util.Date;

import lombok.Data;

@Data
public class ClientDto {

	private int id;
	private String firstname;
	private String lastname;
	private String email;
	private String phone;
	private String address;
	private String city;
	private String postalCode;
	private String country;
	private String password;
	private Date dateCreate;
}
