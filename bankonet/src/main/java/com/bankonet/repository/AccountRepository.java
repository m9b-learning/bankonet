package com.bankonet.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bankonet.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Integer> {

	List<Account> findByClientId(int id);

	Account findByTypeAndClientId(String type, int id);
}
