package com.bankonet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bankonet.entity.Client;

public interface ClientRepository extends JpaRepository<Client, Integer> {

	Client findByEmailAndPassword(String email, String password);
}
