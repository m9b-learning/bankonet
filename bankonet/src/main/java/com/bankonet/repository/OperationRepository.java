package com.bankonet.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import com.bankonet.entity.Operation;

public interface OperationRepository extends JpaRepository<Operation, Integer> {

	List<Operation> findByAccountId(int id, Sort sort);

	void deleteByAccountId(int id);
	
}
