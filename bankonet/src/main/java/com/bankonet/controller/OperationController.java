package com.bankonet.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import com.bankonet.dto.OperationDto;
import com.bankonet.service.OperationService;

@CrossOrigin(origins = { "http://localhost:4200" }, maxAge = 3000)
@Controller
@RequestMapping(path = "/operation")
public class OperationController {

	@Autowired
	private OperationService operationService;

	@GetMapping
	public ResponseEntity<List<OperationDto>> getOperations() {
		List<OperationDto> list = operationService.getOperations();
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@GetMapping(path = "{id}")
	public ResponseEntity<OperationDto> findOperation(@PathVariable("id") int id) {
		OperationDto operation = operationService.findOperation(id);
		return new ResponseEntity<>(operation, HttpStatus.OK);
	}

	@GetMapping(path = "/account/{id}")
	public ResponseEntity<List<OperationDto>> findOperationsByAccountId(@PathVariable("id") int id) {
		List<OperationDto> list = operationService.findOperationsByAccountId(id);
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Void> createOperation(@RequestBody OperationDto operation, UriComponentsBuilder builder) {
		operationService.createOperation(operation);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/operation/{id}").buildAndExpand(operation.getId()).toUri());
		return new ResponseEntity<>(headers, HttpStatus.CREATED);
	}

	@PutMapping
	public ResponseEntity<Void> updateOperation(@RequestBody OperationDto operation) {
		operationService.updateOperation(operation);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@DeleteMapping("{id}")
	public ResponseEntity<Void> deleteOperation(@PathVariable("id") int id) {
		operationService.deleteOperation(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
