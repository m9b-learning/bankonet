package com.bankonet.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import com.bankonet.dto.AccountDto;
import com.bankonet.service.AccountService;

@CrossOrigin(origins = { "http://localhost:4200" }, maxAge = 3000)
@Controller
@RequestMapping(path = "/account")
public class AccountController {

	@Autowired
	private AccountService accountService;

	@GetMapping
	public ResponseEntity<List<AccountDto>> getAccounts() {
		List<AccountDto> list = accountService.getAccounts();
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@GetMapping(path = "{id}")
	public ResponseEntity<AccountDto> findAccount(@PathVariable("id") int id) {
		AccountDto account = accountService.findAccount(id);
		return new ResponseEntity<>(account, HttpStatus.OK);
	}

	@GetMapping(path = "/client/{id}")
	public ResponseEntity<List<AccountDto>> findAccountsByClientId(@PathVariable("id") int id) {
		List<AccountDto> list = accountService.findAccountsByClientId(id);
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
	@GetMapping(path = "/client/current/{id}")
	public ResponseEntity<AccountDto> findCurrentAccountsByClientId(@PathVariable("id") int id) {
		AccountDto list = accountService.findCurrentAccountByClientId(id);
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Void> createAccount(@RequestBody AccountDto account, UriComponentsBuilder builder) {
		accountService.createAccount(account);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/account/{id}").buildAndExpand(account.getId()).toUri());
		return new ResponseEntity<>(headers, HttpStatus.CREATED);
	}

	@PutMapping
	public ResponseEntity<Void> updateAccount(@RequestBody AccountDto account) {
		accountService.updateAccount(account);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@DeleteMapping("{id}")
	public ResponseEntity<Void> deleteAccount(@PathVariable("id") int id) {
		accountService.deleteAccount(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
