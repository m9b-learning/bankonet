package com.bankonet.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import com.bankonet.dto.ClientDto;
import com.bankonet.service.ClientService;

@CrossOrigin(origins = { "http://localhost:4200" }, maxAge = 3000)
@Controller
@RequestMapping(path = "/client")
public class ClientController {

	@Autowired
	private ClientService clientService;

	@GetMapping
	public ResponseEntity<List<ClientDto>> getClients() {
		List<ClientDto> list = clientService.getClients();
		return new ResponseEntity<>(list, HttpStatus.ACCEPTED);
	}

	@GetMapping(path = "{id}")
	public ResponseEntity<ClientDto> findClient(@PathVariable("id") int id) {
		ClientDto clientDto = clientService.findClient(id);
		return new ResponseEntity<>(clientDto, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Void> createClient(@RequestBody ClientDto clientDto, UriComponentsBuilder builder) {
		clientService.createClient(clientDto);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/client/{id}").buildAndExpand(clientDto.getId()).toUri());
		return new ResponseEntity<>(headers, HttpStatus.CREATED);
	}

	@PutMapping
	public ResponseEntity<Void> updateClient(@RequestBody ClientDto clientDto) {
		clientService.updateClient(clientDto);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@DeleteMapping("{id}")
	public ResponseEntity<Void> deleteClient(@PathVariable("id") int id) {
		clientService.deleteClient(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PostMapping("signin")
	public ResponseEntity<ClientDto> authentication(@RequestBody ClientDto clientDto) {
		
		ClientDto connected = clientService.authentication(clientDto);
		if (connected == null) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		return new ResponseEntity<>(connected, HttpStatus.OK);
	}
}
