package com.bankonet.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bankonet.dto.AccountDto;
import com.bankonet.dto.DtoParser;
import com.bankonet.dto.OperationDto;
import com.bankonet.repository.AccountRepository;

@Service
@Transactional
public class AccountService {

	public static final Logger LOG = LoggerFactory.getLogger(AccountService.class);

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private OperationService operationService;

	public List<AccountDto> getAccounts() {
		List<AccountDto> accounts = DtoParser.parseListAccountToDto(accountRepository.findAll());
		accounts.stream().forEach(ac -> ac.setOperations(operationService.findOperationsByAccountId(ac.getId())));

		return accounts;
	}

	public AccountDto findAccount(int id) {
		AccountDto account = DtoParser.parseAccountToDto(accountRepository.getOne(id));
		account.setOperations(operationService.findOperationsByAccountId(account.getId()));

		return account;
	}
	
	public AccountDto findCurrentAccountByClientId(int id) {
		AccountDto account = DtoParser.parseAccountToDto(accountRepository.findByTypeAndClientId("current", id));
		account.setOperations(operationService.findOperationsByAccountId(account.getId()));
		return account;
	}

	public List<AccountDto> findAccountsByClientId(int id) {
		List<AccountDto> accounts = DtoParser.parseListAccountToDto(accountRepository.findByClientId(id));
		accounts.stream().forEach(ac -> ac.setOperations(operationService.findOperationsByAccountId(ac.getId())));

		return accounts;
	}

	public AccountDto createAccount(AccountDto accountDto) {
		accountRepository.save(DtoParser.parseDtoToAccount(accountDto));
		return accountDto;
	}

	public void updateAccount(AccountDto accountDto) {
		findAccount(accountDto.getId());
		accountRepository.save(DtoParser.parseDtoToAccount(accountDto));
	}
	
	public void deleteAccount(int accountId) {
		
		AccountDto accountDto = findAccount(accountId);
		
		List<AccountDto> accounts = findAccountsByClientId(accountDto.getClientId());
		AccountDto currentAccount = accounts.stream().filter(c -> c.getType().equals("current")).findFirst().get();	//findCurrentAccountByClientId(accountDto.getClientId());
		
		List<OperationDto> operations = operationService.findOperationsByAccountId(accountId);
		
		double overdraft = accountDto.getOverdraft();
		double balance = accountDto.getBalance();
		String typeAccountDto = accountDto.getType();
		
		
		
		if (typeAccountDto.equals("saving") ) {
			if (balance >= 0) {
				OperationDto operationDto = new OperationDto();
				
				operationDto.setType("credit");
				operationDto.setAmount(balance);
				operationDto.setLabel("versement compte épargne");
				operationDto.setAccountId(currentAccount.getId());
				
				operationService.createOperation(operationDto);
				
			}
			
			operationService.deleteByAccountId(accountId);
			accountRepository.delete(DtoParser.parseDtoToAccount(accountDto));
			
			AccountService.LOG.info("nouveau solde compte courant: %f", currentAccount.getBalance());
		}
		
		if (typeAccountDto.equals("current") && overdraft >= 0 && accounts.size() == 1) {
			//accountRepository.delete(account); 
		}
			
	}

}
