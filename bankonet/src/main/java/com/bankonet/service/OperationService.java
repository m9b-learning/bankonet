package com.bankonet.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bankonet.dto.DtoParser;
import com.bankonet.dto.OperationDto;
import com.bankonet.entity.Operation;
import com.bankonet.repository.OperationRepository;

@Service
@Transactional
public class OperationService {
	
	public static final Logger LOG = LoggerFactory.getLogger(OperationService.class);

	@Autowired
	private OperationRepository operationRepository;

	public List<OperationDto> getOperations() {
		return DtoParser.parseListOperationToDto(operationRepository.findAll());
	}

	public OperationDto findOperation(int id) {
		Operation operation = operationRepository.getOne(id);
		return DtoParser.parseOperationToDto(operation);
	}

	public List<OperationDto> findOperationsByAccountId(int id) {
		Sort sort = new Sort(Direction.ASC, "dateCreate");
		return DtoParser.parseListOperationToDto(operationRepository.findByAccountId(id, sort));
	}
	
	public OperationDto createOperation(OperationDto operationDto) {
		operationRepository.save(DtoParser.parseDtoToOperation(operationDto));
		return operationDto;
	}

	public void updateOperation(OperationDto operationDto) {
		findOperation(operationDto.getId());
		operationRepository.save(DtoParser.parseDtoToOperation(operationDto));
	}

	public void deleteOperation(int operationId) {
		Operation operation = DtoParser.parseDtoToOperation(findOperation(operationId));
		operationRepository.delete(operation);
	}
	
	public void deleteByAccountId(int accountId) {
		operationRepository.deleteByAccountId(accountId);
	}

}
