package com.bankonet.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bankonet.dto.ClientDto;
import com.bankonet.dto.DtoParser;
import com.bankonet.entity.Client;
import com.bankonet.repository.ClientRepository;

@Service
@Transactional
public class ClientService {
	
    public static final Logger LOG = LoggerFactory.getLogger(ClientService.class);

	@Autowired
	private ClientRepository clientRepository;

	public List<ClientDto> getClients() {
		return DtoParser.parseListClientToDto(clientRepository.findAll());
	}

	public ClientDto findClient(int id) {
		Client client = clientRepository.getOne(id);		
		return DtoParser.parseClientToDto(client);
	}

	public ClientDto createClient(ClientDto clientDto) {
		clientRepository.save(DtoParser.parseDtoToClient(clientDto));
		return clientDto;
	}
	
	public void updateClient(ClientDto clientDto) {
		findClient(clientDto.getId());
		clientRepository.save(DtoParser.parseDtoToClient(clientDto));
	}

	public void deleteClient(int clientId) {
		Client client = DtoParser.parseDtoToClient(findClient(clientId));
		clientRepository.delete(client);
	}

	public ClientDto authentication(ClientDto clientDto) {
		Client client = clientRepository.findByEmailAndPassword(clientDto.getEmail(), clientDto.getPassword());
		return DtoParser.parseClientToDto(client);
	}
	
}
