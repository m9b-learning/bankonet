package com.bankonet.exception;

import javax.persistence.EntityNotFoundException;

import org.omg.CORBA.portable.ApplicationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice("com.bankonet.controller")
public class ApplicationErrorHandler {

	@Value("${err.constraint}")
	private String monMessage;

	@ExceptionHandler(value = { ApplicationException.class })
	public ResponseEntity<ErrorInfo> applicationErrorHandler(Exception e) {
		ErrorInfo retour = new ErrorInfo();
		retour.setCode("400");
		retour.setMessage(e.getMessage());
		return new ResponseEntity<>(retour, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = { org.hibernate.exception.ConstraintViolationException.class })
	public ResponseEntity<ErrorInfo> constrainViolationHandler(Exception e) {
		ErrorInfo retour = new ErrorInfo();
		retour.setCode("500");
		retour.setMessage(monMessage);
		return new ResponseEntity<>(retour, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(value = { EntityNotFoundException.class })
	public ResponseEntity<ErrorInfo> applicationErrorHandler(EntityNotFoundException e) {
		ErrorInfo retour = new ErrorInfo();
		retour.setCode("404");
		retour.setMessage(e.getMessage());
		return new ResponseEntity<>(retour, HttpStatus.NOT_FOUND);
	}

}