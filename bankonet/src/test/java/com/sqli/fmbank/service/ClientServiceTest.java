package com.bankonet.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.omg.CORBA.portable.ApplicationException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bankonet.dto.ClientDto;
import com.bankonet.entity.Client;
import com.bankonet.repository.ClientRepository;

@RunWith(SpringJUnit4ClassRunner.class)
public class ClientServiceTest {

	@InjectMocks
	ClientService clientService;

	@Mock
	ClientRepository clientRepository;

	static Client firstClient;
	static Client secondClient;
	static List<Client> clients;

	@BeforeClass
	public static void beforeClass() {
		firstClient = new Client();
		firstClient.setId(1);
		firstClient.setFirstname("John");
		firstClient.setLastname("Doe");
		firstClient.setEmail("john.doe@mail.com");
		firstClient.setPhone("0102030405");
		firstClient.setAddress("1 rue de la rue");
		firstClient.setPostalCode("33000");
		firstClient.setCity("Bordeaux");
		firstClient.setCountry("France");

		secondClient = new Client();
		secondClient.setId(1);
		secondClient.setFirstname("Jane");
		secondClient.setLastname("Doe");
		secondClient.setEmail("jane.doe@mail.com");
		secondClient.setPhone("0605040302");
		secondClient.setAddress("1 rue de la rue");
		secondClient.setPostalCode("33000");
		secondClient.setCity("Bordeaux");
		secondClient.setCountry("France");

		clients = new ArrayList<>();
		clients.add(firstClient);
		clients.add(secondClient);
	}

	@Before
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getOneClient() throws Exception {

		when(clientRepository.getOne(any(int.class))).thenReturn(firstClient);

		ClientDto client = clientService.findClient(1);

		assertTrue(client != null && client.getId() == 1 && firstClient.getFirstname().equals(client.getFirstname())
				&& firstClient.getLastname().equals(client.getLastname())
				&& firstClient.getEmail().equals(client.getEmail()) && firstClient.getPhone().equals(client.getPhone())
				&& firstClient.getAddress().equals(client.getAddress())
				&& firstClient.getPostalCode().equals(client.getPostalCode())
				&& firstClient.getCity().equals(client.getCity())
				&& firstClient.getCountry().equals(client.getCountry()));

	}

	@Test
	public void getAllClients() throws Exception {

		// WHEN
		when(clientRepository.findAll()).thenReturn(clients);

		List<ClientDto> clientsDto = clientService.getClients();

		// THEN
		assertTrue(clientsDto != null && clientsDto.size() == 2
				&& firstClient.getFirstname().equals(clientsDto.get(0).getFirstname())
				&& secondClient.getFirstname().equals(clientsDto.get(1).getFirstname()));
	}

	@Test
	public void getNoClients() throws Exception {

		// WHEN
		when(clientRepository.findAll()).thenReturn(new ArrayList<Client>());

		List<ClientDto> clientsDto = clientService.getClients();

		// THEN
		assertEquals(0, clientsDto.size());
	}

	@Test(expected = Exception.class)
	public void getNoClient() throws Exception {

		// WHEN
		when(clientRepository.getOne(any(int.class))).thenReturn(null);

		ClientDto clientDto = clientService.findClient(1);

		// THEN
		fail("Une exception aurait du être levée");
	}

	@Test
	public void getCreateClient() throws Exception {

		// WHEN
		when(clientRepository.save(any(Client.class))).thenReturn(firstClient);
		ClientDto clientDto = new ClientDto();
		clientDto.setEmail("suzane.doe@mail.com");
		clientDto.setPhone("0605040302");
		clientDto.setFirstname("Suzane");
		clientDto.setLastname("Doe");

		clientDto = clientService.createClient(clientDto);

		// THEN
		verify(clientRepository, times(1)).save(any(Client.class));

	}
}
