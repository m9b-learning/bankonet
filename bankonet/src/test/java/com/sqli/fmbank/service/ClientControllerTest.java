package com.bankonet.service;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import javax.persistence.EntityNotFoundException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bankonet.controller.ClientController;
import com.bankonet.dto.ClientDto;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class ClientControllerTest {

private MockMvc mockMvc;
    
    @InjectMocks
    ClientController clientController;
    
    @Mock
    ClientService clientService;
    
    static ClientDto client;
	
    @BeforeClass
    public static void beforeClass(){
        client = new ClientDto();
        client.setId(1);
        client.setFirstname("John");
        client.setLastname("Doe");
        client.setEmail("john.doe@mail.com");
        client.setPhone("0605040302");

    }

    @Before
    public void beforeTests(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(clientController).build();
     }
    
//    @Test
//    public void findOneOk() throws Exception {
//        when(clientService.getClient(1)).thenReturn(client);
//        
//        MvcResult result = mockMvc.perform(get("/client/1")).andExpect(status().isOk()).andReturn();
//        
//        assertTrue(result != null &&
//                "{\"id\":1,\"lastname\":\"Doe\",\"firstname\":\"John\",\"email\":\"john.doe@mail.com\",\"phone\":\"0605040302\"}"
//        		.equals(result.getResponse().getContentAsString()));
//    }

    @Test
    public void findOneKo() throws EntityNotFoundException {
        when(clientService.findClient(any(int.class))).thenThrow(new EntityNotFoundException("Client inexistant"));
        
        try {
            mockMvc.perform(get("/client/1"));
            fail("En exception aurait du être levée");
        }
        catch (Exception e) {
            assertTrue(e.getCause() instanceof EntityNotFoundException &&
                    "Client inexistant".equals(((EntityNotFoundException) e.getCause()).getMessage())
                    );
         }
        
    }
}
