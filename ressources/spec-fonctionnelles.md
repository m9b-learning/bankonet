# SPEC FONCTIONNELLES

### Client

- Se connecter

- Lister l'ensemble de ses comptes

- Editer son profil

- Faire des opérations courantes sur ses comptes (débit/crédit)

- Faire des virements

  - interne : débit/crédit

  - externe : débit/crédit uniquement de compte courant à compte courant

- Gérer ses comptes épargnes

  - Créer un compte épargne

  - supprimer un compte épargne : le solde doit automatiquement être viré vers le compte courant
